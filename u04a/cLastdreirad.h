#pragma once
#include "cNutzrad.h"



class cLastdreirad :
	public cNutzrad
{
private:
	double nutzlast;
public:
	cLastdreirad(double nutz_in = 72.50, double ertrag_in = 380.00, int rad_in = 3, double druck_in = 3.8);

	double zuladen(double lastplus);
	double abladen(double lastminus);
};

