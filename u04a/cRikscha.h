#pragma once
#include "cNutzrad.h"



class cRikscha :
	public cNutzrad
{
private:
	int fahrgastzahl;
public:
	cRikscha(int gast_in = 1, double ertrag_in = 620.00, int rad_in = 4, double druck_in = 2.7);

	int einsteigen(int rein);
	int aussteigen(int raus);
};

