#include "cEinrad.h"

cEinrad::cEinrad(double spass_in, int rad_in, double druck_in)
	: cFreizeitrad(rad_in, druck_in, spass_in)
{
}

double cEinrad::geniessen(double genuss)
{
	if (genuss >= 0.0)
		spass += genuss * 20;
	else
		spass += genuss * 5;

	return spass;
}
