#include <iostream>
#include "cLastdreirad.h"
#include "cRikscha.h"
#include "cMountainbike.h"
#include "cEinrad.h"

using namespace std;

int main() 
{
	cLastdreirad hauruck; 
	cRikscha einergehtnochrein; 
	cMountainbike downhillstar; 
	cEinrad obenbleiben;

	cout << "Radzahl des Lastdreirads: " << hauruck.get_radzahl() << endl;
	cout << "Radzahl der Rikscha: " << einergehtnochrein.get_radzahl() << endl;
	cout << "Radzahl des Mountainbikes: " << downhillstar.get_radzahl() << endl;
	cout << "Radzahl des Einrads: " << obenbleiben.get_radzahl() << endl;

	cout << "Mountainbike nach 300 meter downhill, spass = " << downhillstar.downhill(-300) << endl;

	cout << "Mountainbike bekommt einen Steinschlag" << endl;
	downhillstar.steinschlag();
	cout << "Mountainbike nach Steinschlag, spass = " << downhillstar.downhill(0) << endl;

	cout << "Mountainbike bekommt einen Steinschlag" << endl; downhillstar.steinschlag();
	cout << "Mountainbike nach Steinschlag, spass = " << downhillstar.downhill(0) << endl;

	cout << "Ende" << endl;

	return 0;
}
