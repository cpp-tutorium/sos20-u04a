#include "cNutzrad.h"

cNutzrad::cNutzrad(int rad_in, double druck_in, double ertrag_in)
	: cFahrrad(rad_in, druck_in), ertrag(ertrag_in)
{
}

double cNutzrad::kasieren(double einkommen)
{
	ertrag += einkommen;
	return ertrag;
}

double cNutzrad::wartungmachen(double kosten)
{
	ertrag -= kosten;
	return ertrag;
}
