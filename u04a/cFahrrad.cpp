#include "cFahrrad.h"

cFahrrad::cFahrrad(int radzahl_in, double luftdruck_in)
	: radzahl(radzahl_in), luftdruck(luftdruck_in)
{
}

int cFahrrad::get_radzahl()
{
	return radzahl;
}

double cFahrrad::get_luftdruck()
{
	return luftdruck;
}

double cFahrrad::aufpumpen(double druckplus)
{
	if ((luftdruck + druckplus) > 3.5)
		luftdruck += druckplus;
	else
		luftdruck = 3.5;

	return luftdruck;
}
