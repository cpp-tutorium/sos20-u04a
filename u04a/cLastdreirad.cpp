#include "cLastdreirad.h"

cLastdreirad::cLastdreirad(double nutz_in, double ertrag_in, int rad_in, double druck_in)
	: cNutzrad(rad_in, druck_in, ertrag_in), nutzlast(nutz_in)
{
}

double cLastdreirad::zuladen(double lastplus)
{
	nutzlast += lastplus;
	return nutzlast;
}

double cLastdreirad::abladen(double lastminus)
{
	nutzlast -= lastminus;
	return nutzlast;
}
