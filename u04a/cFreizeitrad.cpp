#include "cFreizeitrad.h"

cFreizeitrad::cFreizeitrad(int rad_in, double druck_in, double spass_in)
	: cFahrrad(rad_in, druck_in), spass(spass_in)
{
}

void cFreizeitrad::abschliessen(bool disziplin)
{
	if (disziplin)
		spass += 20.0;
	else
		spass = 0.0;
}

double cFreizeitrad::geniessen(double genuss)
{
	spass += genuss;
	return spass;
}
