#pragma once
#include "cFreizeitrad.h"



class cEinrad :
	public cFreizeitrad
{
private:

public:
	cEinrad(double spass_in = 200.0, int rad_in = 1, double druck_in = 2.3);

	double geniessen(double genuss) override;
};

