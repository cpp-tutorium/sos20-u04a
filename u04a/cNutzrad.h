#pragma once
#include "cFahrrad.h"


class cNutzrad :
	public cFahrrad
{
private:
	double ertrag;
public:
	cNutzrad(int rad_in, double druck_in, double ertrag_in);

	double kasieren(double einkommen);
	double wartungmachen(double kosten);
};

