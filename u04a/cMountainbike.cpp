#include "cMountainbike.h"
#include <iostream>

cMountainbike::cMountainbike(double spass_in, int rad_in, double druck_in)
	: cFreizeitrad(rad_in, druck_in, spass_in)
{
}

double cMountainbike::downhill(int hoehendifferenz)
{
	spass -= 10.0 * hoehendifferenz;
	return spass;
}

void cMountainbike::steinschlag()
{
	if ((spass - 2000.0) >= 0)
		spass -= 2000.0;
	else
		spass = 0.0;

	schimpfen();
}

void cMountainbike::schimpfen()
{
	std::cout << "Schon wieder ein Stein, macht keinen Spass mehr" << std::endl;
}
