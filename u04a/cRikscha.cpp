#include "cRikscha.h"

cRikscha::cRikscha(int gast_in, double ertrag_in, int rad_in, double druck_in)
	: cNutzrad(rad_in, druck_in, ertrag_in), fahrgastzahl(gast_in)
{
}

int cRikscha::einsteigen(int rein)
{
	if ((fahrgastzahl + rein) <= 7)
		fahrgastzahl += rein;
	else
		fahrgastzahl = 7;

	return fahrgastzahl;
}

int cRikscha::aussteigen(int raus)
{
	if ((fahrgastzahl - raus) >= 0)
		fahrgastzahl -= raus;
	else
		fahrgastzahl = 0;

	return fahrgastzahl;
}
