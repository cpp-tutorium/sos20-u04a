#pragma once
#include "cFahrrad.h"



class cFreizeitrad :
	public cFahrrad
{
protected:
	double spass;
public:
	cFreizeitrad(int rad_in, double druck_in, double spass_in);

	void abschliessen(bool disziplin);
	virtual double geniessen(double genuss);
};

