#pragma once
#include "cFreizeitrad.h"



class cMountainbike :
	public cFreizeitrad
{
public:
	cMountainbike(double spass_in = 100.0, int rad_in = 2, double druck_in = 1.3);

	double downhill(int hoehendifferenz);
	void steinschlag();

private:
	void schimpfen();
};

